/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Employee;

/**
 *
 * @author TAO
 */
public class EmployeeDao implements DaoInterface<Employee>{

    @Override
    public int add(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO employee (name,tel) VALUES (?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,object.getName());
            stmt.setString(2,object.getTel());            
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();            
            if(result.next()){
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("Error: add "+ex.getMessage());
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Employee> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel FROM employee";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");            
                Employee employee = new Employee(id,name,tel);
                list.add(employee);
            }
        } catch (SQLException ex) {
            System.out.println("Error: getAll"+ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public Employee get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel FROM employee WHERE id="+id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int pid = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");              
                Employee employee = new Employee(pid,name,tel);
                return employee;
            }
        } catch (SQLException ex) {
            System.out.println("Error: get id "+id+" "+ex.getMessage());
        }   
        return null;
    }

    @Override
    public int Delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM employee WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,id);
            row = stmt.executeUpdate();           
        } catch (SQLException ex) {
            System.out.println("Error: delete "+ex.getMessage());
        }
        db.close();
        return row;
    }

    @Override
    public int update(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE employee SET name = ?,tel = ? WHERE id = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,object.getName());
            stmt.setString(2,object.getTel());           
            stmt.setInt(3,object.getId());
            row = stmt.executeUpdate();                               
        } catch (SQLException ex) {
            System.out.println("Error: update "+ex.getMessage());
        }
        db.close();
        return row;
    }
       
    public ArrayList<Employee> getNameorTel(String word) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel FROM employee WHERE name LIKE"+"\'"+word+"%"+"\'"+"OR tel Like"+"\'"+word+"%"+"\'";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");            
                Employee employee = new Employee(id,name,tel);
                list.add(employee);
            }
        } catch (SQLException ex) {
            System.out.println("Error: getName"+ex.getMessage());
        }
        db.close();
        return list;
    }
}
