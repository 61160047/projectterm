/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Order;
import model.Receipt;
import model.ReceiptDetail;
import model.User;

/**
 *
 * @author TAO
 */
public class OrderDao implements DaoInterface<Receipt>{

    @Override
    public int add(Receipt object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO receipt (total,user_id,customer_id) VALUES (?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1,object.getTotal());
            stmt.setInt(2,object.getSeller().getId());   
            stmt.setInt(3,object.getCustomer().getId());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();            
            if(result.next()){
                id = result.getInt(1);
            }
            for(ReceiptDetail a:object.getReceiptDetail()){
            String sqle = "INSERT INTO receipt_datail (price,amount,receipt_id,product_id) VALUES (?,?,?,?)";
            PreparedStatement stmtr = conn.prepareStatement(sqle);
            stmtr.setDouble(1,a.getTotal());
            stmtr.setInt(2,a.getAmount());   
            stmtr.setInt(3,a.getReceipt().getId());
            stmtr.setInt(4,a.getProduct().getId());
            int rowa = stmtr.executeUpdate();
            ResultSet resulta = stmtr.getGeneratedKeys();            
            if(resulta.next()){
                id = result.getInt(1);
            }
            }
        } catch (SQLException ex) {
            System.out.println("Error: add "+ex.getMessage());
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,\n" +
                    "created,\n" +
                    "total,\n" +
                    "user_id,\n" +
                    "customer_id,\n" +
                    "promotion_id\n" +
                    "FROM receipt";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("id");
                double total = result.getDouble("total");
                int userid = result.getInt("user_id");
                int cusid = result.getInt("customer_id");
                Receipt receipt = new Receipt(id,total,new User(userid),new Customer(cusid));
                list.add(receipt);
            }
        } catch (SQLException ex) {
            System.out.println("Error: getAll"+ex.getMessage());
        } 
        db.close();
        return list;
    }

    @Override
    public Receipt get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "id,\n" +
                    "created,\n" +
                    "total,\n" +
                    "user_id,\n" +
                    "customer_id,\n" +
                    "promotion_id\n" +
                    "FROM receipt WHERE id="+id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int pid = result.getInt("id");
                int amount = result.getInt("amount");
                double total = result.getDouble("total");            
                Receipt receipt = new Receipt(id);
                return receipt;
            }
        } catch (SQLException ex) {
            System.out.println("Error: get id "+id+" "+ex.getMessage());
        }   
        return null;
    }

    @Override
    public int Delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM receipt WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,id);
            row = stmt.executeUpdate();           
        } catch (SQLException ex) {
            System.out.println("Error: delete "+ex.getMessage());
        }
        db.close();
        return row;
    }

    @Override
    public int update(Receipt object) {       
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
             String sql = "UPDATE receipt SET total = ? WHERE id = ? ";
             PreparedStatement stmt = conn.prepareStatement(sql);
             stmt.setDouble(1,object.getTotal());                        
             stmt.setInt(2,object.getId());
             row = stmt.executeUpdate();                               
         } catch (SQLException ex) {
             System.out.println("Error: update "+ex.getMessage());
         }
         db.close();
         return row;
    }
    
    public ArrayList<Receipt> getID(String word) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id FROM receipt WHERE id LIKE"+"\'"+word+"%"+"\'";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("id");
                //int amount = result.getInt("amount");
                //double total = result.getDouble("total");            
                Receipt receipt = new Receipt(id);
                list.add(receipt);
            }
        } catch (SQLException ex) {
            System.out.println("Error: getName"+ex.getMessage());
        }
        db.close();
        return list;
    }
    
    
    public static void main(String[] args) {
       OrderDao dao = new OrderDao();
        System.out.println(dao.getAll());;             
    }
//    @Override
//    public int update(Receipt object) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
}
