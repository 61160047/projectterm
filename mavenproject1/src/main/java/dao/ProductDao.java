/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;


/**
 *
 * @author KoonAoN
 */
public class ProductDao implements DaoInterface<Product>{

    @Override
    public int add(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO product (name,price) VALUES (?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,object.getName());
            stmt.setDouble(2,object.getPrice());            
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();            
            if(result.next()){
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("Error: add "+ex.getMessage());
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Product> getAll() {
        ArrayList<Product> list = new ArrayList<>();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        try {
            String sql = "SELECT * FROM product";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){              
                int Prod_ID = result.getInt("id");
                String Prod_Name = result.getString("name");
                double Prod_Price = result.getDouble("price");
                String Prod_Image = result.getString("image");
                Product product = new Product(Prod_ID,Prod_Name,Prod_Price,Prod_Image);
                list.add(product);
            }
        } catch (SQLException ex) {
            System.out.println("Error: getAll"+ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public Product get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try{
            String sql = "SELECT * FROM product;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int pid = result.getInt("id");
                String name = result.getString("name");
                Double price = result.getDouble("price");
                String image = result.getString("image");
                Product product = new Product(pid,name,price,"");
                return product;
            }
        } catch (SQLException ex) {
            System.out.println("Error: get id "+id+" "+ex.getMessage());
        }   
        return null;
    }

    @Override
    public int Delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM product WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,id);
            row = stmt.executeUpdate();           
        } catch (SQLException ex) {
            System.out.println("Error: delete "+ex.getMessage());
        }
        db.close();
        return row;
    }

    @Override
    public int update(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE product SET name = ?,price = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,object.getName());
            stmt.setDouble(2,object.getPrice());           
            stmt.setInt(3,object.getId());
            row = stmt.executeUpdate();                               
        } catch (SQLException ex) {
            System.out.println("Error: update "+ex.getMessage());
        }
        db.close();
        return row;
    }
    
    public ArrayList<Product> getData(String word) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT * FROM product WHERE name LIKE"+"\'"+word+"%"+"\'"+"OR price Like"+"\'"+word+"%"+"\'";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("id");
                String name = result.getString("name");
                Double price = result.getDouble("price");  
                String image = result.getString("image");
                Product product = new Product(id,name,price,image);
                list.add(product);
            }
        } catch (SQLException ex) {
            System.out.println("Error: getName"+ex.getMessage());
        }
        db.close();
        return list;
    }
    public static void main(String[] args) {
        ProductDao dao = new ProductDao();
        System.out.println(dao.get(1));
        int id = dao.add(new Product(-1,"Green Tea",30,""));
        System.out.println(dao.getAll());
    }
}
