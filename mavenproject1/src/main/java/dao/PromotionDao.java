/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Employee;
import model.Promotion;

/**
 *
 * @author TAO
 */
public class PromotionDao implements DaoInterface<Promotion>{

    @Override
    public int add(Promotion object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO promotion (name,start,end) VALUES (?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,object.getName());
            stmt.setString(2,object.getStart());  
            stmt.setString(3,object.getEnd());   
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();            
            if(result.next()){
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("Error: add "+ex.getMessage());
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Promotion> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,start,end FROM promotion";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("id");
                String name = result.getString("name");
                String start = result.getString("start"); 
                String end = result.getString("end");
                Promotion promotion = new Promotion(id,name,start,end);
                list.add(promotion);
            }
        } catch (SQLException ex) {
            System.out.println("Error: getAll"+ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public Promotion get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,start,emd FROM promotion WHERE id="+id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int pid = result.getInt("id");
                String name = result.getString("name");
                String start = result.getString("string"); 
                String end = result.getString("end");
                Promotion promotion = new Promotion(id,name,start,end);
                return promotion;
            }
        } catch (SQLException ex) {
            System.out.println("Error: get id "+id+" "+ex.getMessage());
        }   
        return null;
    }

    @Override
    public int Delete(int id) {
         Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM promotion WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,id);
            row = stmt.executeUpdate();           
        } catch (SQLException ex) {
            System.out.println("Error: delete "+ex.getMessage());
        }
        db.close();
        return row;
    }

    @Override
    public int update(Promotion object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE promotion SET name = ?,start = ?,end = ? WHERE id = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,object.getName());
            stmt.setString(2,object.getStart());   
            stmt.setString(3,object.getEnd());   
            stmt.setInt(4,object.getId());
            row = stmt.executeUpdate();                               
        } catch (SQLException ex) {
            System.out.println("Error: update "+ex.getMessage());
        }
        db.close();
        return row;
    } 
    public ArrayList<Promotion> getIDorName(String word) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,start,end FROM promotion WHERE id LIKE"+"\'"+word+"%"+"\'"+"OR name Like"+"\'"+word+"%"+"\'";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("id");
                String name = result.getString("name");
                String start = result.getString("start"); 
                String end = result.getString("end");
                Promotion promotion = new Promotion(id,name,start,end);
                list.add(promotion);
            }
        } catch (SQLException ex) {
            System.out.println("Error: getName"+ex.getMessage());
        }
        db.close();
        return list;
    }
}
