/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import static java.lang.Double.parseDouble;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Employee;
import model.Staple;
import model.Stock;

/**
 *
 * @author TAO
 */
public class StapleDao implements DaoInterface<Staple>{

    public int add(Staple object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO staple (name,price) VALUES (?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,object.getName());
            stmt.setDouble(2,object.getPrice());            
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();            
            if(result.next()){
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("Error: add "+ex.getMessage());
        }
        db.close();
        return id;
    }
    
    @Override
    public ArrayList<Staple> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,price FROM staple";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("id");
                String name = result.getString("name");
                double price = result.getDouble("price");            
                Staple staple = new Staple(id,name,price);
                list.add(staple);
            }
        } catch (SQLException ex) {
            System.out.println("Error: getAll"+ex.getMessage());
        }
        db.close();
        return list;
    }
    
  @Override
    public Staple get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,price FROM staple WHERE id= "+id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int pid = result.getInt("id");
                String name = result.getString("name");
                double price = result.getDouble("price");              
                Staple staple = new Staple(pid,name,price);
                return staple;
            }
        } catch (SQLException ex) {
            System.out.println("Error: get id "+id+" "+ex.getMessage());
        }   
        return null;
    }  
    
    @Override
    public int Delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM staple WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,id);
            row = stmt.executeUpdate();           
        } catch (SQLException ex) {
            System.out.println("Error: delete "+ex.getMessage());
        }
        db.close();
        return row;
    }
    
    @Override
    public int update(Staple object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE staple SET name = ?,price = ? WHERE id = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,object.getName());
            stmt.setDouble(2,object.getPrice());           
            stmt.setInt(3,object.getId());
            row = stmt.executeUpdate();                               
        } catch (SQLException ex) {
            System.out.println("Error: update "+ex.getMessage());
        }
        db.close();
        return row;
    }
    
     public ArrayList<Staple> getNameorID(String keyword) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,price FROM staple WHERE name LIKE"+"\'"+keyword+"%"+"\'"+"OR ID Like"+"\'"+keyword+"%"+"\'";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("id");
                String name = result.getString("name");
                String price = result.getString("price");            
                Staple employee = new Staple(id,name,parseDouble(price));
                list.add(employee);
            }
        } catch (SQLException ex) {
            System.out.println("Error: getName"+ex.getMessage());
        }
        db.close();
        return list;
    }
      
     public static void main(String[] args) {
        StapleDao dao = new StapleDao();
        System.out.println(dao.get(1));
        int id = dao.add(new Staple(-1,"Tea powder",150));
        System.out.println(dao.getAll());             
    }
}
