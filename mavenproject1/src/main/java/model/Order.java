/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author TAO
 */
public class Order {
    private int id;
    private int amount;
    private double total;

    public Order(int id, int amount, double total) {
        this.id = id;
        this.amount = amount;
        this.total = total;
    }
    
    public Order(int amount, double total) {
        this(-1,amount,total);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "Order{" + "id=" + id + ", amount=" + amount + ", total=" + total + '}';
    }
       
}
