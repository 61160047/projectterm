/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import dao.ProductDao;
import java.util.ArrayList;

/**
 *
 * @author KoonAoN
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private String image;
    private int amount;

    public Product(int id, String name, double price,String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    
    public Product(String name,double price){
        this(-1,name,price,"");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
    @Override
    public String toString() {
        return ""+id;
    }
    public String toString2() {
        return name;
    }
    public String toString3() {
        return ""+price;
    }
    public static ArrayList<Product> genProductList(ProductDao ProductClass) {
        ArrayList<Product> list = ProductClass.getAll();
        //* data of product *//       
      //*list.add(new ProductClass(1, "Espresso", 40.00, "1.Jpg"));
     //*list.add(new ProductClass(2, "Late", 40.00, "2.Jpg"));
      //*   list.add(new ProductClass(3, "Chocolate", 30.00, "3.Jpg"));
      //*   list.add(new ProductClass(4, "Thai Tea", 40.00, "4.Jpg"));
      //*   list.add(new ProductClass(5, "Green Tea", 40.00, "5.Jpg"));
        
        return list;
    }
}
