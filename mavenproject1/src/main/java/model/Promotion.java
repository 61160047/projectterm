/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author TAO
 */
public class Promotion {
    private int id;
    private String name;
    private String start;
    private String end;

    public Promotion(int id, String name, String start ,String end) {
        this.id = id;
        this.name = name;
        this.start = start;
        this.end = end;
    }
    
    public Promotion(String name, String start ,String end) {
        this(-1,name,start,end);
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "Promotion{" + "id=" + id + ", name=" + name + ", start=" + start + ", end=" + end + '}';
    }     
}
