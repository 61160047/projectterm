/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;
import model.Customer;
import model.Product;
import model.User;

/**
 *
 * @author NonKirito
 */
public class Receipt {
    private int id;
    private Date created;
    private User seller;
    private Customer customer;
    private double total;
    private ArrayList<ReceiptDetail> receiptDetail;

    public Receipt(int id, Date created, User seller, Customer customer) {
        this.id = id;
        this.created = created;
        this.seller = seller;
        this.customer = customer;
        receiptDetail = new ArrayList();
    }
    public Receipt(int id) {
        this.id = id;
        receiptDetail = new ArrayList();
    }
       
    public Receipt(User seller, Customer customer) {
        this(-1, null,seller, customer);
    }

    public Receipt(int id,double total, User seller, Customer customer) {
        this.id = id;
        this.total = total;
        this.seller = seller;
        this.customer = customer;
        receiptDetail = new ArrayList();
        
    }
    public void addReceiptDetail(int id, Product product, int amount, double price){
        for(int row = 0; row < receiptDetail.size(); row++){
            ReceiptDetail r = receiptDetail.get(row);
            if(r.getProduct().getId() == product.getId()){
                r.addAmount(amount);
                return;
            }            
        }
        receiptDetail.add(new ReceiptDetail(id, product, amount, price, this));

    }
    public void addReceiptDetail(Product product, int amount){
        addReceiptDetail(-1, product, amount, product.getPrice());
    }
    public void addReceiptDetail01(Product product){
        receiptDetail.add(new ReceiptDetail(-1, product, this));
    }
    public void deleteRecieptDetail(int row){
        receiptDetail.remove(row);
    }
    public void setTotal(double total){
        this.total = total;
    }
    public double getTotal(){
//        double total = 0;
//        for(ReceiptDetail r: receiptDetail){
//            total += r.getTotal();
//        }
        return total;
    }
    public int getId() {
        return id;
    }

    public Date getCreated() {
        return created;
    }

    public User getSeller() {
        return seller;
    }

    public Customer getCustomer() {
        return customer;
    }

    public ArrayList<ReceiptDetail> getReceiptDetail() {
        return receiptDetail;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setReceiptDetail(ArrayList<ReceiptDetail> receiptDetail) {
        this.receiptDetail = receiptDetail;
    }

   @Override
    public String toString() {
        String str = "Receipt{" + "id=" + id 
                + ", created=" + created 
                + ", seller=" + seller 
                + ", customer=" + customer 
                + ", total=" + total
                +"}\n";
        for(ReceiptDetail r: receiptDetail){
            str += r.toString()+"\n";
        }
        return str;
    }



    
}
