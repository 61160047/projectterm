/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author TAO
 */
public class StockDetail {
    private int id;
    private Product product;
    private Stock stock;
    private double price;
    private int amount;

    public StockDetail(int id, Product product, Stock stock, double price, int amount) {
        this.id = id;
        this.product = product;
        this.stock = stock;
        this.price = price;
        this.amount = amount;
    }
    
    public StockDetail(Product product, Stock stock, double price, int amount) {
        this(-1,product,stock,price,amount);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "StockDetail{" + "id=" + id + ", product=" + product + ", stock=" + stock + ", price=" + price + ", amount=" + amount + '}';
    }
       
}
